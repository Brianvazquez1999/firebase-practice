// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import {getFirestore} from "firebase/firestore"
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyCcxJ-ULi-9w9mhd3BGrRrwzvLUCmdYfmw",
  authDomain: "moviereservation-7bf6d.firebaseapp.com",
  projectId: "moviereservation-7bf6d",
  storageBucket: "moviereservation-7bf6d.appspot.com",
  messagingSenderId: "529237021596",
  appId: "1:529237021596:web:e59d27edeacaee3c942e59",
  measurementId: "G-YLK76CBBDC"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);
export const db = getFirestore(app)
