import { getDocs, collection } from "firebase/firestore"
import {db }from "../firebase.js"
import {useEffect, useState} from "react"
import "../App.js"

function MovieList () {
    const [movies, setMovies] = useState([])
    const [registered, setRegistered] = useState({})
    const [hovered, setHovered] = useState(false)
    const movieCollectionRef = collection(db, "movies")
    const handleMouseEnter = () => {
        setHovered(true)
    }
    const handleMoustLeave = () =>{
        setHovered(false)
    }

    const handleClick = (name, showing) => {
        setRegistered((registered) =>( {...registered, [name]: showing}))
    }
    console.log(registered)
    const timeStyle = {
        background: hovered ? "orange" : "transparent",
    }
    useEffect(() => {
        const getMovies = async ()=> {
            try {
                const data = await getDocs(movieCollectionRef);
                const movieData = data.docs.map((docs) => ({...docs.data(), id: docs.id}))

                setMovies(movieData)
            }
            catch (err) {
                console.error(err);
            }


        }

        getMovies();
    }, []);
    const movieUrl = movies[0]
    var date = new Date(0)
    if (movies.length !== 0) {
         return (
            <div>
            <h1 style={{textAlign:"center"}}>Movies</h1>
        <div className="grid-container">
            {movies.map((movie) => {
                return (
                <>
                <div className="movie-container">
                <img src={movie.photourl} style={{ width:"100%", height: "100%", objectFit: "cover"}} />
                <div className="movie-times" ><h2 style={{textAlign:"center"}}>Register for a time</h2>
                {movie.showing.map((showing) => {
                    {date = new Date(0)}
                    {date.setSeconds(showing.seconds)}
                    {date.setMilliseconds(showing.nanoseconds/1000000000)}
                    console.log(movie.name)
                    return (
                        <>
                        <a className="a"><h2 onClick={() =>handleClick(movie.name, showing)} className="time" style={{textAlign:"center", ...timeStyle}}>{date.toLocaleString()}</h2></a>
                        </>
                    )
                })}
                </div>
               </div>
                </>
                )

            })}
        </div>
        </div>

)
    }


}

export default MovieList;
